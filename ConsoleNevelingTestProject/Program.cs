﻿using NevelingTestProjectLibrary.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleNevelingTestProject
{
    class Program
    {
        static bool fin = false;

        static void Main(string[] args)
        {
            TestCrawler testCrawler = new TestCrawler();

            testCrawler.CrawlerDepth = 2;
            testCrawler.NumOfConcurrentThreads = 10;
            testCrawler.OutputDirectory = "c:\\Neveling\\WeltTest";
            testCrawler.Roots = new string[] { @"http://www.welt.de" };
            testCrawler.AllowCompression = false;
            testCrawler.DataCanged += new EventHandler<MyEventArgs>(crawler_DataCanged);
            testCrawler.Finished += new EventHandler<FinishedDownloadingEventArgs>(crawler_Finished);

            //Time for thread to sleep when there is no url's to handle
            testCrawler.ThreadIdleTime = 500;

            //Time for thread to sleep after downloading a page
            testCrawler.ThreadSleepTime = 500;

            //Check connection to internet
            if (!testCrawler.IsConnectedToInternet())
            {
                Console.WriteLine("Not connected to the internet");
                return;
            }

            testCrawler.Go();

            while (fin == false)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        #region events
        static void crawler_Finished(object sender, FinishedDownloadingEventArgs e)
        {
            fin = true;
        }

        static void crawler_DataCanged(object sender, MyEventArgs e)
        {
            Console.WriteLine(e.Message);
        }
        #endregion
    }
}
