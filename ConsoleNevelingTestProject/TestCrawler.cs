﻿using NevelingTestProjectLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleNevelingTestProject
{
    public class TestCrawler : GenericCrawler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageData"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        protected override List<string> ExtractLinks(string pageData, string url)
        {
            List<string> links = BuiltInExtractLinks(pageData, url);
            //Do some sort of logic here
            return links;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        protected override bool Filter(string url)
        {
            url = url.ToLower();
            if (url.Contains("search?") && url.Contains("&start="))
                return false;

            return true;
        }
    }
}
