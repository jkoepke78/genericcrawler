﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevelingTestProjectLibrary.DTO
{
    public struct UrlModel
    {
        private string name;
        private int depth;

        public string Name { set { name = value; } get { return name; } }
        public int Depth { set { depth = value; } get { return depth; } }
    }
}
