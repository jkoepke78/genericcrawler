﻿using NevelingTestProjectLibrary.DTO;
using NevelingTestProjectLibrary.Events;
using NevelingTestProjectLibrary.Network;
using NevelingTestProjectLibrary.Regex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace NevelingTestProjectLibrary
{
    public abstract class GenericCrawler
    {
        //Creating the extern function for checking connection
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        #region member variables
        public event EventHandler<MyEventArgs> DataCanged;
        public event EventHandler<FinishedDownloadingEventArgs> Finished;

        private bool allowCompression = true;
        private string characterSet = "";
        private string[] roots;
        private int crawlerDepth;
        private int numOfConcurrentThreads;
        private int threadIdleTime = 1000;
        private int threadSleepTime = 0;
        private string outputDirectory;
        #endregion


        #region static variables
        //True when there is no urls in the queue list and all threads had finished their work
        private static bool Exit;

        //Incremented each time a thread is starting to download , when reached to 0 and queue list is empty the proccess has finished
        private static int workingThreads = 0;
        #endregion


        #region properties
        //Set to false to avoid errors when compressed sites doesnt use a valid gzip compression
        public bool AllowCompression { set { allowCompression = value; } get { return allowCompression; } }

        //Set default characterset . if none specified than the one on the httpresponse is used
        public string CharacterSet { set { characterSet = value; } get { return characterSet; } }

        //Accepting several root urls
        public string[] Roots { set { roots = value; } get { return roots; } }

        //Setting crawler depth
        public int CrawlerDepth { set { crawlerDepth = value; } get { return crawlerDepth; } }
        
        //Number of simultaneous threads 
        public int NumOfConcurrentThreads { set { numOfConcurrentThreads = value; } get { return numOfConcurrentThreads; } }

        //Sleeping time for threads that dont have work
        public int ThreadIdleTime { set { threadIdleTime = value; } get { return threadIdleTime; } }

        //Sleeping time after a thread done downloading
        public int ThreadSleepTime { set { threadSleepTime = value; } get { return threadSleepTime; } }

        public List<string[]> DownloadedUrls { get { return downloadedUrls; } }

        //Output directory to save the pages
        public string OutputDirectory { set { outputDirectory = value; } get { return outputDirectory; } }

        protected virtual bool Filter(string url) { return false; }
        #endregion


        #region member functions
        private Thread[] threadList;

        //The url list that will be populated by the threads
        private static Queue<UrlModel> queuedUrls = new Queue<UrlModel>();

        //A timer that check when all the threads had finished their work and the url list is empty
        private System.Timers.Timer checkQueuedListTimer = new System.Timers.Timer();

        // Holds Downloaded url info  [originalUrl , relativeLocalUri , absoluteLocalUri]
        private List<string[]> downloadedUrls = new List<string[]>();

        public void Go()
        {
            if (roots.Length == 0)
                return;
            else
                Exit = false;

            //Feed roots to queue list
            foreach (string root in roots)
                queuedUrls.Enqueue(new UrlModel { Name = root, Depth = 1 });


            //Set Threads List
            if (numOfConcurrentThreads <= 0)
                threadList = new Thread[10];
            else
                threadList = new Thread[numOfConcurrentThreads];

            for (int i = 0; i < threadList.Length; i++)
            {
                threadList[i] = new Thread(ThreadExecute);
                threadList[i].Start(i.ToString());
            }

            //Set Timer
            checkQueuedListTimer.Interval = 500;
            checkQueuedListTimer.Elapsed += new System.Timers.ElapsedEventHandler(checkQueuedListTimer_Elapsed);
            checkQueuedListTimer.Enabled = true;
        }

        void checkQueuedListTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (queuedUrls)
            {

                if (queuedUrls.Count == 0 && workingThreads == 0)
                {
                    Finished(this, new FinishedDownloadingEventArgs(downloadedUrls));
                    Exit = true;
                    checkQueuedListTimer.Enabled = false;
                }
            }
        }

        public void ThreadExecute(object threadId)
        {
            string id = (string)threadId;
            while (Exit == false)
            {
                UrlModel? myurl = null;
                lock (queuedUrls)
                {
                    if (queuedUrls.Count > 0)
                        myurl = queuedUrls.Dequeue();
                }

                if (myurl != null)
                    CrawlPage(myurl, id);
                else
                    Thread.Sleep(threadIdleTime);
            }
        }

        public void CrawlPage(object obj, string threadId)
        {
            try
            {
                workingThreads++;

                MyWebClient wc = new MyWebClient();
                UrlModel url = (UrlModel)obj;

                System.Diagnostics.Debug.WriteLine("Thread number " + threadId + " starting to download " + DateTime.Now.ToLongTimeString());
                string pageData = wc.DownloadString(url.Name, allowCompression, characterSet);
                System.Diagnostics.Debug.WriteLine("Thread number " + threadId + " finished downloading " + DateTime.Now.ToLongTimeString());

                string directory = IO.File.UrlToUnc(url.Name);
                string file = IO.File.PageFromUrl(url.Name);
                string relativePath = SpecialRegex.ToValidWindowsPath(directory, file + ".html");

                IO.File.SaveFileToDisk(outputDirectory + "\\" + relativePath, pageData);

                if (DataCanged != null)
                    DataCanged(this, new MyEventArgs("Downloaded Page " + url.Name));

                downloadedUrls.Add(new string[] { url.Name, relativePath, outputDirectory + "\\" + relativePath });

                // Check if reached depth , dont crawl this page
                if (url.Depth == crawlerDepth)
                    return;

                //Continue Crawling
                List<string> links = ExtractLinks(pageData, url.Name);
                List<string> approvedLinks = new List<string>();

                string tempUrl;
                foreach (string link in links)
                {
                    tempUrl = System.Web.HttpUtility.HtmlDecode(link);
                    if (UrlExists(tempUrl) | Filter(tempUrl))
                        continue;
                    else
                        approvedLinks.Add(tempUrl);
                }

                if (approvedLinks.Count > 0)
                {
                    lock (queuedUrls)
                    {
                        foreach (string link in approvedLinks)
                            queuedUrls.Enqueue(new UrlModel { Depth = url.Depth + 1, Name = link });
                    }
                }

                //Sleep before trying to handle another request
                Thread.Sleep(threadSleepTime);
            }
            catch (Exception e)
            {
                if (DataCanged != null) DataCanged(this, new MyEventArgs(e.Message));
            }
            finally
            {
                workingThreads--;
            };

        }

        public bool IsConnectedToInternet()
        {
            int Desc;
            return InternetGetConnectedState(out Desc, 0);
        }

        private bool UrlExists(string url)
        {
            foreach (string[] s in downloadedUrls)
                if (s[0].Equals(url))
                    return true;
            return false;
        }

        protected List<string> BuiltInExtractLinks(string pageData, string url)
        {
            List<string> list = new List<string>();
            MatchCollection matches = SpecialRegex.Matches(pageData, SpecialRegex.URLPATTERN, RegexOptions.Singleline);

            string baseUrl = SpecialRegex.GetBase(url);

            foreach (Match m in matches)
            {
                string uri = m.Value;
                uri = SpecialRegex.cleanUrl(uri);
                uri = SpecialRegex.Absolutize(baseUrl, uri);
                list.Add(uri);
            }

            return list;
        }

        protected virtual List<string> ExtractLinks(string pageData, string url)
        {
            return BuiltInExtractLinks(pageData, url);
        }
        #endregion
    }
}
