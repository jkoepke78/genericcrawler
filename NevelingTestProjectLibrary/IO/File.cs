﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevelingTestProjectLibrary.IO
{
    public static class File
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileData"></param>
        public static void SaveFileToDisk(string filePath, string fileData)
        {
            try
            {
                string dir = Path.GetDirectoryName(filePath);

                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                if (filePath != "")
                    System.IO.File.WriteAllText(filePath, fileData);

            }
            catch (Exception e) { throw new Exception("Error in file : " + e.Message); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string UrlToUnc(string url)
        {
            string directoryPath;

            directoryPath = System.Text.RegularExpressions.Regex.Replace(url, "[^/]*$", "");
            directoryPath = System.Text.RegularExpressions.Regex.Replace(directoryPath, "^(ht|f)tp(s?):", "");
            directoryPath = System.Text.RegularExpressions.Regex.Replace(directoryPath, @"/", @"\");

            return directoryPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string PageFromUrl(string url)
        {
            string file = System.Text.RegularExpressions.Regex.Match(url, @"[^/]*$").Value;
            return file;
        }
    }
}
