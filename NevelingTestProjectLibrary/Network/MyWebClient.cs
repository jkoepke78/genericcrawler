﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NevelingTestProjectLibrary.Network
{
    public class MyWebClient : WebClient
    {
        private const int BUFFERSIZE = 8196;

        public byte[] DownloadData(string url, bool allowCompression)
        {
            try
            {
                HttpWebRequest hreq = (HttpWebRequest)WebRequest.Create(url);
                hreq.AllowAutoRedirect = true;
                HttpWebResponse response = (HttpWebResponse)hreq.GetResponse();
                string encoding = response.CharacterSet;
                hreq.Abort();
                response.Close();

                //Check response
                switch (response.StatusCode)
                {
                    case HttpStatusCode.Accepted: break;
                    case HttpStatusCode.OK: break;
                    case HttpStatusCode.Found: break;
                    default:
                        throw new Exception(response.StatusCode.ToString() + " url : " + url);
                }

                WebHeaderCollection headers = new WebHeaderCollection();
                headers.Add(HttpRequestHeader.Accept, "*/*");
                headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                headers.Add(HttpRequestHeader.UserAgent, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; InfoPath.2)");
                if (allowCompression)
                    headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                this.Headers = headers;

                if (encoding != "")
                    this.Encoding = System.Text.Encoding.GetEncoding(encoding);
                byte[] byteArray = this.DownloadData(url);


                CompressedMethod method = CompressedMethod.None;

                if (ResponseHeaders["Content-Encoding"] != null)
                    Compresser.CompressMethod(ResponseHeaders["Content-Encoding"]);

                if (method != CompressedMethod.None)
                    return Compresser.Decompress(byteArray, method);

                return byteArray;
            }
            catch (Exception e) { throw e; }
        }

        public string DownloadString(string url, bool allowCompression, string charSet)
        {
            try
            {
                HttpWebRequest hreq = (HttpWebRequest)WebRequest.Create(url);
                hreq.AllowAutoRedirect = true;
                HttpWebResponse response = (HttpWebResponse)hreq.GetResponse();
                string encoding = response.CharacterSet;
                hreq.Abort();
                response.Close();

                //Check response
                switch (response.StatusCode)
                {
                    case HttpStatusCode.Accepted: break;
                    case HttpStatusCode.OK: break;
                    case HttpStatusCode.Found: break;
                    default:
                        throw new Exception(response.StatusCode.ToString() + " url : " + url);
                }

                WebHeaderCollection headers = new WebHeaderCollection();
                headers.Add(HttpRequestHeader.Accept, "*/*");
                headers.Add(HttpRequestHeader.UserAgent, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; InfoPath.2)");

                if (charSet != "")
                    headers.Add(HttpRequestHeader.ContentType, "charset=" + charSet);
                else if (encoding != "")
                    headers.Add(HttpRequestHeader.ContentType, "charset=" + encoding);

                if (allowCompression)
                    headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                this.Headers = headers;

                string data = this.DownloadString(url);


                //Try getting compressed data
                CompressedMethod method = CompressedMethod.None;

                if (ResponseHeaders["Content-Encoding"] != null)
                {
                    method = Compresser.CompressMethod(ResponseHeaders["Content-Encoding"]);
                }

                if (method != CompressedMethod.None)
                {
                    byte[] arr;
                    arr = Compresser.Decompress(System.Text.Encoding.ASCII.GetBytes(data), method);
                    return System.Text.Encoding.ASCII.GetString(arr);
                }

                return data;
            }
            catch (Exception e) { throw e; }
        }

        public long GetContentLength(string sourceByUrl)
        {
            WebResponse response = this.GetWebResponse(WebRequest.Create(sourceByUrl));
            long len = response.ContentLength;
            response.Close();
            return len;

        }
    }
}
