﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevelingTestProjectLibrary.Network
{
    public static class Compresser
    {
        private const int BUFFERSIZE = 8192;

        public static CompressedMethod CompressMethod(string val)
        {
            val = val.ToLower();
            switch (val)
            {
                case "gzip":
                    return CompressedMethod.GZip;
                case "deflate":
                    return CompressedMethod.Deflate;
                default:
                    return CompressedMethod.None;
            }
        }

        public static byte[] Decompress(byte[] byteArray, CompressedMethod method)
        {
            try
            {
                byte[] buffer = new byte[BUFFERSIZE];
                MemoryStream streamin = new MemoryStream();
                MemoryStream streamout = new MemoryStream();
                streamin.Write(byteArray, 0, byteArray.Length);
                streamin.Position = 0;

                Stream compresser;

                switch (method)
                {
                    case CompressedMethod.GZip:
                        compresser = new GZipStream(streamin, CompressionMode.Decompress);
                        break;
                    case CompressedMethod.Deflate:
                        compresser = new DeflateStream(streamin, CompressionMode.Decompress);
                        break;
                    default:
                        return null;
                }

                int ByteRead = compresser.Read(buffer, 0, BUFFERSIZE);

                while (ByteRead > 0)
                {
                    streamout.Write(buffer, 0, ByteRead);
                    ByteRead = compresser.Read(buffer, 0, BUFFERSIZE);
                }

                return streamout.ToArray();
            }
            catch { throw new Exception("Error Decompressing Data"); };
        }
    }

    public enum CompressedMethod { GZip, Deflate, None };
}
