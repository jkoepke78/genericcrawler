﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevelingTestProjectLibrary.Regex
{
    public class SpecialRegex : System.Text.RegularExpressions.Regex
    {
        public const string URLPATTERN = @"(href|Href)=""[\d\w\/:#@%;$\(\)~_\?\+\-=\\\.&]*";

        public static string GetExtension(string str)
        {
            if (str.EndsWith(@"/"))
                return @"/";

            string Ext = Match(str, @"[^\.]*$").Value;

            return Ext;
        }

        public static string GetDirectory(string str)
        {
            return Match(str, @"http://[^/]*").Value;
        }

        public static string GetBase(string url)
        {
            return Replace(url, @"[^/]*$", "");
        }

        public static string Absolutize(string baseUrl, string relativeUrl)
        {
            if (!IsMatch(relativeUrl, "^(http|https|ftp)"))
                return baseUrl + relativeUrl;
            return relativeUrl;
        }

        public static string ToValidWindowsFileName(string str)
        {
            // Replace unvalid charecters
            str = Replace(str, "[\\/:*?\"<>|]+", "_", System.Text.RegularExpressions.RegexOptions.Singleline);
            return str;
        }

        public static string ToValidWindowsPath(string directory, string file)
        {
            string newfile = ToValidWindowsFileName(file);
            string fullPath = directory + newfile;

            if (fullPath.Length > 250)
            {
                if (directory.Length > 230)
                    return "";

                int fileNewLength = 248 - directory.Length; //minimum 18 characters

                if (newfile.Length > 10)
                    newfile = newfile.Substring(0, 10);

                Random rand = new Random();
                fullPath = directory + newfile + rand.Next(0, 400000).ToString();
                return fullPath;
            }
            return fullPath;
        }

        public static string cleanUrl(string url)
        {
            return Replace(url, @"[Hh]ref=|""", "");
        }
    }
}
