﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevelingTestProjectLibrary.Events
{
    public class MyEventArgs : EventArgs
    {
        #region member variables
        private string msg;
        #endregion


        #region constructor
        public MyEventArgs(string messageData)
        {
            msg = messageData;
        }
        #endregion


        #region properties
        public string Message
        {
            get { return msg; }
            set { msg = value; }
        }
        #endregion
    }
}
