﻿using System;
using System.Collections.Generic;


namespace NevelingTestProjectLibrary.Events
{
    public class FinishedDownloadingEventArgs : EventArgs
    {
        #region member variables
        private List<string[]> downloadedList;
        #endregion


        #region constructor
        public FinishedDownloadingEventArgs(List<string[]> messageData)
        {
            downloadedList = messageData;
        }
        #endregion


        #region properties
        public List<string[]> DownloadedList
        {
            get { return downloadedList; }
            set { downloadedList = value; }
        }
        #endregion
    }
}
